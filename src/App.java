import java.io.Console;
import java.util.ArrayList;
import java.util.Random;

class App {
  private Console console = System.console();
  private Cell[][] board;
  private int width;
  private int height;

  App(int width, int height) {
    this.width = width;
    this.height = height;
    this.board = new Cell[height][width];
  }

  void start() {
    if (this.console == null) {
      System.err.println("No console.");
      System.exit(1);
    }
    this.generateCells();
    this.randomizeLife();
    this.printCells();
    while (true) {
      this.console.readLine();
      this.doLoop();
    }
  }

  private void doLoop() {
    this.dieAndSpawn();
    this.prune();
    this.printCells();
  }

  private void printCells() {
    ArrayList<String> twoD = new ArrayList<>();
    for (Cell[] cells: this.board) {
      StringBuilder row = new StringBuilder();
      for (Cell cell: cells) {
        row.append(cell.alive ? "*" : "-");
      }
      twoD.add(row.toString());
    }
    for (String row: twoD) {
      System.out.println(row);
    }
  }

  private void generateCells() {
    for (int y = 0; y < this.height; y++) {
      Cell[] bXList = new Cell[this.width];
      for (int x = 0; x < this.width; x++) {
        bXList[x] = new Cell(x, y);
      }
      this.board[y] = bXList;
    }
  }

  private void randomizeLife() {
    Random gen = new Random();
    for (int i = 0; i < 250; i++) {
      int rx = gen.nextInt(width);
      int ry = gen.nextInt(height);
      this.board[ry][rx].alive = true;
    }
  }

  private void prune() {
    for (Cell[] row: this.board) {
      for (Cell cell : row) {
        cell.alive = cell.willLive;
      }
    }
  }

  private boolean cellIsAlive(int x, int y) {
    if (y >= 0 && y < height && x >= 0 && x < width) {
      return this.board[y][x].alive;
    }
    else {
      return false;
    }
  }

  private void dieAndSpawn() {
    for (Cell[] row: this.board) {
      for (Cell cell: row) {
        int numNeighbors = 0;
        int x = cell.x;
        int y = cell.y;

        Integer[][] locations = {
          {(x - 1), (y - 1)}, {x, (y - 1)}, {(x + 1), (y - 1)},
          {(x - 1),  y     },               {(x + 1),  y     },
          {(x - 1), (y + 1)}, {x, (y + 1)}, {(x + 1), (y + 1)}
        };

        for (Integer[] location: locations) {
          if (this.cellIsAlive(location[0], location[1])) {
            numNeighbors++;
          }
        }

        if (cell.alive) {
          if (numNeighbors < 2) this.board[y][x].willLive = false;
          if (numNeighbors > 3) this.board[y][x].willLive = false;
        }
        if (!cell.alive) {
          if (numNeighbors == 3) this.board[y][x].willLive = true;
        }
      }
    }
  }
}
