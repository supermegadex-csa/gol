public class Cell {
  int x;
  int y;
  boolean alive = false;
  boolean willLive = false;

  Cell(int x, int y) {
    this.x = x;
    this.y = y;
  }
}